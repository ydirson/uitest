#!/usr/bin/python3 -Xdev -Wdefault

# FIXME:
# - nodes not removed
# - Qt5, no ChildrenChanged signal for dialogs or frames ?

import asyncio
from enum import Enum
import logging

from dbus_next.aio import MessageBus

__all__ = (
    "atspibus get_desktop"
).split(' ')

logger = logging.getLogger("a14y")

INTROSPECTION = {}
with open("at-spi2-core/xml/Registry.xml", "r") as f:
    INTROSPECTION['Registry'] = f.read()
    assert not f.read()

for key in "Desktop Application Accessible Cache".split(' '):
    with open(f"data/{key}.xml", "r") as f:
        INTROSPECTION[key] = f.read()
        assert not f.read()

async def open_atspibus():
    "Locate the bus for the main AT-SPI traffic"
    sessionbus = await MessageBus().connect()

    # hardcoded in at-spi2-core/bus/at-spi-bus-launcher.c
    a11y_bus_introspection = (
        "<node>"
        "  <interface name='org.a11y.Bus'>"
        "    <method name='GetAddress'>"
        "      <arg type='s' name='address' direction='out'/>"
        "    </method>"
        "  </interface>"
        "    <interface name='org.a11y.Status'>"
        "    <property name='IsEnabled' type='b' access='readwrite'/>"
        "    <property name='ScreenReaderEnabled' type='b' access='readwrite'/>"
        "  </interface>"
        "</node>"
    )

    a11y_bus = sessionbus.get_proxy_object('org.a11y.Bus', '/org/a11y/bus',
                                           a11y_bus_introspection)

    a11y_status_iface = a11y_bus.get_interface('org.a11y.Status')
    await a11y_status_iface.set_is_enabled(False)
    is_enabled = await a11y_status_iface.get_is_enabled()
    if not is_enabled:
        logger.warning("enabling a11y first")
        await a11y_status_iface.set_is_enabled(True)
        is_enabled = await a11y_status_iface.get_is_enabled()
        assert is_enabled

    a11y_bus_iface = a11y_bus.get_interface('org.a11y.Bus')
    a11y_busname = await a11y_bus_iface.call_get_address()
    return await MessageBus(a11y_busname).connect()

class Cache(object):
    def __init__(self, bus, busname):
        self.obj = bus.get_proxy_object(
            busname, '/org/a11y/atspi/cache',
            INTROSPECTION['Cache'])
        self.cache = self.obj.get_interface('org.a11y.atspi.Cache')
        self.items = None

    async def get_items(self):
        if self.items is None:
            self.items = await self.cache.call_get_items()
        return self.items

class Accessible(object):
    Child = None # would set to Accessible if it was already defined at this point
    def __init__(self, bus, address, parent, *, introspectkey='Accessible', autoloadchildren=True):
        assert isinstance(address, list) and len(address) == 2
        self.bus = bus
        self.address = address
        self.parent = parent
        self.app = parent.app if parent else None
        self.role, self.name = None, None
        self.obj = bus.get_proxy_object(self.address[0], self.address[1],
                                        INTROSPECTION[introspectkey])
        self.accessible = self.obj.get_interface('org.a11y.atspi.Accessible')
        self.event = self.obj.get_interface('org.a11y.atspi.Event.Object')
        self.children = None
        self.states = None
        self.attributes = None
        self.cache = Cache(bus, address[0])
        self.__autoloadchildren = autoloadchildren

        # register into app's cache
        if self.app:
            self.app.cache_register(self)

    async def async_init(self):
        await self.get_role()
        await self.get_name()
        if self.__autoloadchildren:
            await self.get_children()
        #print("cache for", self, ":", await self.cache.get_items())
        return self

    def __await__(self):
        return self.async_init().__await__()

    Role = Enum('Accessible.Role', """ACCELERATOR_LABEL ALERT
        ANIMATION ARROW CALENDAR CANVAS CHECK_BOX CHECK_MENU_ITEM
        COLOR_CHOOSER COLUMN_HEADER COMBO_BOX DATE_EDITOR DESKTOP_ICON
        DESKTOP_FRAME DIAL DIALOG DIRECTORY_PANE DRAWING_AREA
        FILE_CHOOSER FILLER FOCUS_TRAVERSABLE FONT_CHOOSER FRAME
        GLASS_PANE HTML_CONTAINER ICON IMAGE INTERNAL_FRAME LABEL
        LAYERED_PANE LIST LIST_ITEM MENU MENU_BAR MENU_ITEM
        OPTION_PANE PAGE_TAB PAGE_TAB_LIST PANEL PASSWORD_TEXT
        POPUP_MENU PROGRESS_BAR PUSH_BUTTON RADIO_BUTTON
        RADIO_MENU_ITEM ROOT_PANE ROW_HEADER SCROLL_BAR SCROLL_PANE
        SEPARATOR SLIDER SPIN_BUTTON SPLIT_PANE STATUS_BAR TABLE
        TABLE_CELL TABLE_COLUMN_HEADER TABLE_ROW_HEADER
        TEAROFF_MENU_ITEM TERMINAL TEXT TOGGLE_BUTTON TOOL_BAR
        TOOL_TIP TREE TREE_TABLE UNKNOWN VIEWPORT WINDOW EXTENDED
        HEADER FOOTER PARAGRAPH RULER APPLICATION AUTOCOMPLETE EDITBAR
        EMBEDDED ENTRY CHART CAPTION DOCUMENT_FRAME HEADING PAGE
        SECTION REDUNDANT_OBJECT FORM LINK INPUT_METHOD_WINDOW
        TABLE_ROW TREE_ITEM DOCUMENT_SPREADSHEET DOCUMENT_PRESENTATION
        DOCUMENT_TEXT DOCUMENT_WEB DOCUMENT_EMAIL COMMENT LIST_BOX
        GROUPING IMAGE_MAP NOTIFICATION INFO_BAR LEVEL_BAR TITLE_BAR
        BLOCK_QUOTE AUDIO VIDEO DEFINITION ARTICLE LANDMARK LOG
        MARQUEE MATH RATING TIMER STATIC MATH_FRACTION MATH_ROOT
        SUBSCRIPT SUPERSCRIPT DESCRIPTION_LIST DESCRIPTION_TERM
        DESCRIPTION_VALUE FOOTNOTE CONTENT_DELETION CONTENT_INSERTION
        MARK SUGGESTION""")

    State = Enum('Accessible.State',"""
        ACTIVE ARMED BUSY CHECKED COLLAPSED DEFUNCT EDITABLE ENABLED
        EXPANDABLE EXPANDED FOCUSABLE FOCUSED HAS_TOOLTIP HORIZONTAL
        ICONIFIED MODAL MULTI_LINE MULTISELECTABLE OPAQUE PRESSED
        RESIZABLE SELECTABLE SELECTED SENSITIVE SHOWING SINGLE_LINE
        STALE TRANSIENT VERTICAL VISIBLE MANAGES_DESCENDANTS
        INDETERMINATE REQUIRED TRUNCATED ANIMATED INVALID_ENTRY
        SUPPORTS_AUTOCOMPLETION SELECTABLE_TEXT IS_DEFAULT VISITED
        CHECKABLE HAS_POPUP READ_ONLY""")

    async def get_role(self, force=False):
        if force or self.role is None:
            self.role = Accessible.Role(await self.accessible.call_get_role())
        return self.role

    async def get_name(self, force=False):
        if force or self.name is None:
            self.name = await self.accessible.get_name()
        return self.name

    async def get_children(self, force=False):
        ChildClass = self.Child or Accessible
        if self.children is None:
            self.event.on_children_changed(self.on_children_changed)
        if force or self.children is None:
            self.children = [(self.app.localcache[hashable_address]
                              if self.app and hashable_address in self.app.localcache
                              else await ChildClass(self.bus, address, parent=self))
                             for address, hashable_address in
                             ((address, tuple(address))
                              for address in await self.accessible.call_get_children())]
            #print(self, "children now", [str(c) for c in self.children])
        return self.children

    async def on_children_changed(self, op, index_in_parent, _, v, rest):
        assert v.signature == '(so)'
        child_address = v.value
        ChildClass = self.Child or Accessible
        logger.debug(f"{self}: children_changed {op} #{index_in_parent} child={child_address} rest={rest}")
        if op == 'add':
            self.children.insert(index_in_parent, await ChildClass(self.bus, child_address, parent=self))
            # cache_register called in ctor, maybe FIXME, see get_children
        if op == 'remove':
            child = self.children[index_in_parent]
            assert child.address == child_address
            if self.app:
                self.app.cache_unregister(child)
            self.children[index_in_parent:index_in_parent+1] = []
        logger.debug(f"{self}: children now {self.children!r}")

    async def get_states(self, force=False):
        if force or self.states is None:
            self.states = []
            for fieldnum, field32 in enumerate(await self.accessible.call_get_state()):
                for bit in range(32):
                    if field32 & 1:
                        self.states.append(Accessible.State(bit + 32 * fieldnum))
                    field32 = field32 >> 1
                assert field32 == 0, f"{field32:x} != 0"
        return self.states

    async def get_attributes(self, force=False):
        if force or self.attributes is None:
            self.attributes = await self.accessible.call_get_attributes()
        return self.attributes

    def __repr__(self):
        return f"<{type(self).__name__}({self.address!r})>"

    def __str__(self):
        return f"<{type(self).__name__}({self.address!r}) role={self.role} name={self.name!r}>"

class Application(Accessible):
    def __init__(self, bus, address, parent, *, autoloadchildren=False):
        super().__init__(bus, address, parent,
                         introspectkey='Application', autoloadchildren=autoloadchildren)
        self.app = self
        self.application = self.obj.get_interface('org.a11y.atspi.Application')
        self.toolkit_name, self.version = None, None
        self.localcache = {}

    async def get_toolkit_name(self):
        if not self.toolkit_name:
            self.toolkit_name = await self.accessible.get_toolkit_name()
        return self.toolkit_name

    async def get_toolkit_version(self):
        if not self.toolkit_version:
            self.toolkit_version = await self.accessible.get_version()
        return self.toolkit_version

    def cache_register(self, accessible):
        path = accessible.address[1]
        assert path not in self.localcache
        self.localcache[path] = accessible

    def cache_unregister(self, accessible):
        path = accessible.address[1]
        assert path in self.localcache
        del self.localcache[path]

    def __str__(self):
        return f"<{type(self).__name__}({self.address[0]!r}) name={self.name!r}>"

class Registry(object):
    def __init__(self, bus):
        self.bus = bus
        self.obj = bus.get_proxy_object(
            'org.a11y.atspi.Registry', '/org/a11y/atspi/registry',
            INTROSPECTION['Registry'])
        self.registry = self.obj.get_interface('org.a11y.atspi.Registry')

class Desktop(Accessible):
    Child = Application
    def __init__(self, bus):
        super().__init__(bus, ['org.a11y.atspi.Registry', '/org/a11y/atspi/accessible/root'],
                         parent=None, introspectkey='Desktop')
        self.__app_add_futures = {}
        self.__app_remove_futures = {}

    async def on_children_changed(self, op, index_in_parent, _, v, rest):
        logger.debug(f"Desktop.on_children_changed({op})")
        await super().on_children_changed(op, index_in_parent, _, v, rest)
        app_address = v.value
        if op == "add":
            app = self.children[index_in_parent]
            assert app.address == app_address
            if app.name in self.__app_add_futures:
                self.__app_add_futures[app.name].set_result(app)
                del self.__app_add_futures[app.name]
        if op == "remove":
            hashable_address = tuple(app_address)
            if hashable_address in self.__app_remove_futures:
                self.__app_remove_futures[hashable_address].set_result(True)
                del self.__app_remove_futures[hashable_address]

    async def notify_app_add(self, appname):
        assert appname not in self.__app_add_futures
        self.__app_add_futures[appname] = asyncio.get_running_loop().create_future()
        return await self.__app_add_futures[appname]
    async def notify_app_remove(self, app):
        hashable_address = tuple(app.address)
        assert hashable_address not in self.__app_remove_futures
        self.__app_remove_futures[hashable_address] = asyncio.get_running_loop().create_future()
        return await self.__app_remove_futures[hashable_address]

class Atspi(object):
    def __init__(self):
        self.bus = None
        self.registry = None
        self.desktop = None

    async def get_desktop(self):
        if self.desktop:
            return self.desktop

        self.bus = await open_atspibus()
        self.registry = Registry(self.bus)
        await self.registry.registry.call_register_event("object")
        self.desktop = Desktop(self.bus)
        return self.desktop

if __name__ == '__main__':
    async def main():
        atspi = Atspi()
        desktop = await atspi.get_desktop()
        for app in await desktop.get_children():
            if await app.get_name() == "omaha":
                logger.debug("%s: %s %s %s", app, await app.get_role(), await app.get_name(), await app.get_children())

                # test for signals
                await asyncio.sleep(1000)
                #while True:
                #    await asyncio.sleep(5)
                #    await app.get_children(force=True)
                break
        else:
            logger.critical("no omaha found")

    logging.basicConfig(
        level=logging.DEBUG,
        format='{asctime}|{levelname}|{name}: {message}', style='{')
    logging.getLogger("asyncio").setLevel(logging.DEBUG)

    asyncio.get_event_loop().set_debug(True)
    asyncio.get_event_loop().run_until_complete(main())
