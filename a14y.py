#!/usr/bin/python3 -Xdev -Wdefault

import gi
gi.require_version('Atspi', '2.0')
from gi.repository import Atspi

import logging
import os

import asyncio
# asyncio/glib binding - choose one
if False:
    import asyncio_glib
    asyncio.set_event_loop_policy(asyncio_glib.GLibEventLoopPolicy())
    def set_result(future, value):
        asyncio.get_running_loop().call_soon_threadsafe(future.set_result, value)
else:
    import gbulb
    gbulb.install()
    def set_result(future, value):
        future.set_result(value)

logger = logging.getLogger("a14y")

class Asynccessible(object):
    """Encapsulate an Atspi Accessible with asyncio support.

    We don't rely on identity of atspi.Accessible object, since (as of
    2.36.0) the python objects are not 1-1 mappings to the UI object,
    and we cannot match eg. a children-changed:remove event to the
    corresponding children-changed:add event.

    D-BUS object paths OTOH are really stable.

    Also caches information that becomes unavailable after node death
    (role, name).
    """
    def __init__(self, atspi_mgr, accessible):
        self.atspi_mgr = atspi_mgr
        self.accessible = accessible
        self.path = accessible.path
        self.role, self.name = accessible.get_role(), accessible.get_name()

        if accessible is atspi_mgr.desktop:
            self.parent = None
        else:
            acc_parent = accessible.get_parent()
            if acc_parent in (None, atspi_mgr.desktop):
                self.parent = None
            else:
                self.parent = (self.atspi_mgr._accessibles[acc_parent.path]
                               if acc_parent.path else None)
        self.nodepath = atspi_mgr._nodePath(self.parent, accessible)

        self.visible = accessible.get_state_set().contains(Atspi.StateType.VISIBLE)
        self._hide_fut = asyncio.get_running_loop().create_future()

    def __str__(self):
        return "[{0.role.value_nick} {0.path} {1:x} | {0.name}]".format(self, id(self))

    async def wait_hide(self):
        logger.debug("{0}.wait_hide()".format(self))
        return await self._hide_fut
    def publish_hide(self, accessible):
        assert self.path == accessible.path
        self.visible = False
        logger.debug("{0}.publish_hide()".format(self))
        set_result(self._hide_fut, True)

    def doAction(self, action):
        ai = self.accessible.get_action_iface()
        for i in range(ai.get_n_actions()):
            if ai.get_action_name(i) == action:
                logger.info("action '{}' on {}".format(action, self))
                asyncio.get_running_loop().call_soon_threadsafe(Atspi.Action.do_action, ai, i)
                break
        else:
            logger.warning("action '%s' not found", action)

class Pattern(object):
    def __init__(self, atspi_mgr, pattern):
        """Pattern looking like `pattern` in app managed by `atspi_mgr`.

        `pattern` is a nodepath-like list whose successive items are
        matched against successive items of an `Asynccessible.nodepath`.
        Valid pattern items are:

        `(role, name)`: match an item in a `nodepath` with given role
        and name (both `role` and `name` may be specified as `None` to
        match any value in `nodepath`).

        `None`: match zero or more successive items in a `nodepath`.
        """
        self.atspi_mgr = atspi_mgr
        self.pattern = pattern
        self._show_fut = asyncio.get_running_loop().create_future()

    @classmethod
    def __pattern_match(cls, listpat, nodepath):
        "Internal helper to avoid building sub-Pattern objects"
        if listpat == []:
            return nodepath == [] # end recursion
        pattern_item = listpat[0]

        if isinstance(pattern_item, tuple):
            if len(nodepath) < 1:
                return False

            path_item = nodepath[0]
            assert len(path_item) <= 3, "path item %s too long" % (path_item,)
            path_role, path_name = path_item[0:2]
            if len(path_item) > 2:
                path_labels = [item.name for item in path_item[2]]
            else:
                path_labels = []

            assert len(pattern_item) <= 3, "pattern item %s too long" % (pattern_item,)
            pattern_role, pattern_name = pattern_item[0:2]
            if len(pattern_item) > 2:
                pattern_label = pattern_item[2]
            else:
                pattern_label = None

            if (
                    pattern_role not in (path_role, None) or
                    pattern_name not in (path_name, None) or
                    pattern_label not in path_labels + [None]
            ):
                return False
            return cls.__pattern_match(listpat[1:], nodepath[1:])

        elif pattern_item is None:
            # non-greedy wildcard
            if cls.__pattern_match(listpat[1:], nodepath):
                return True
            elif nodepath == []:
                return False
            else:
                return cls.__pattern_match(listpat, nodepath[1:])

        else:
            assert False, "unrecognized pattern element %s" % (pattern_item,)

    async def wait_show(self):
        logger.debug("{0}.wait_show()".format(self))
        result = await self._show_fut
        self.atspi_mgr.forget_pattern(self)
        return result
    def publish_show(self, ae):
        logger.debug("{0}.publish_show()".format(self))
        set_result(self._show_fut, ae)

    def check_match(self, ae):
        logger.debug("{}.check_match({})".format(
            self, self.atspi_mgr._printable_nodepath(ae.nodepath)))
        if not self.__pattern_match(self.pattern, ae.nodepath):
            return False
        logger.debug("%s %s was awaited for", ae.path, self)
        # is it visible already ?
        if ae.visible:
            self.publish_show(ae)
        return True

    def __str__(self):
        return "Pattern({})".format(self.atspi_mgr._printable_nodepath(self.pattern))

class AppManager(object):
    "A single-app bridge between asyncio and gi.repository.Atspi"
    def __init__(self, appname):
        self.desktop = Atspi.get_desktop(0)

        self.appname = appname
        self.app = None
        self.tk = None
        self.app_terminated = asyncio.get_running_loop().create_future()
        self._accessibles = {}
        self.__expected_patterns = set()

        listener = Atspi.EventListener.new(self._eventWrapper, self._atspiHandler)
        Atspi.EventListener.register(listener, 'object')

    def asynccessible(self, accessible):
        return self._accessibles[accessible.path]

    def _eventWrapper(self, event, callback):
        return callback(event)

    @staticmethod
    def __accessible_relations(accessible, reltype):
        relation_set = accessible.get_relation_set()
        for relation in relation_set:
            if relation.get_relation_type() == reltype:
                labellers = []
                for i in range(relation.get_n_targets()):
                    labellers.append(relation.get_target(i))
                return labellers
        return ()

    def _nodePath(self, ae, accessible=None):
        if accessible is self.app:
            # app is root node
            assert ae is None
            return []
        if ae is None or ae.accessible is self.app:
            ae_nodepath = []
        else:
            ae_nodepath = ae.nodepath
        if accessible:
            acc_labellers = self.__accessible_relations(accessible, Atspi.RelationType.LABELLED_BY)
            labellers = [self.asynccessible(acc) for acc in acc_labellers]
            item = ( (accessible.get_role(), accessible.get_name()) +
                     ((labellers,) if labellers else ()) )
            return ae_nodepath + [item]
        else:
            return ae_nodepath

    def _registerAccessible(self, accessible):
        """Register an Asynccessible for `accessible`, and possibly for its parents.

        Gtk3 and Qt5+ do not seem to advertize new toplevels, so we
        have to discover them from information fragments.
        """
        path = accessible.path
        assert path not in self._accessibles

        states = accessible.get_state_set()
        if states.contains(Atspi.StateType.DEFUNCT):
            logger.debug("ignoring dead-born %s", path)
            return
        # cache all children from highest not-yet-seen in the hierarchy

        name = accessible.get_name()
        role = accessible.get_role()
        parent = accessible.get_parent()
        if parent in (None, self.desktop):
            parent_path = None
        else:
            parent_path = parent.path
            if parent and parent is not self.app and parent_path not in self._accessibles:
                logger.debug("parent recursion for %s %s: %s",
                             path, self.printable_accessible(accessible),
                             self.printable_accessible(parent))
                self._registerAccessible(parent)
                # we've cached this one when recursing downwards from parent
                assert path in self._accessibles, "%r %s not in %s" % (name, path, self._accessibles.keys())
                return

        # only register here if parent is already known

        ae = Asynccessible(self, accessible)
        self._accessibles[path] = ae

        for pattern in self.__expected_patterns:
            pattern.check_match(ae)

        # cache all children
        nchildren = accessible.get_child_count()
        logger.debug("%s registering %s children...", path, nchildren)
        # racy "child" API, collect list of children ASAP before iterating
        for i, child in [ (i, accessible.get_child_at_index(i)) for i in range(nchildren) ]:
            childpath = child.path
            logger.debug("%s/%s: %s...", i+1, nchildren, childpath)
            if child.path not in self._accessibles:
                self._registerAccessible(child)


    def _atspiHandler(self, event):
        if not event.type.startswith("object:"):
            logger.debug("event: %s", event.type)

        if event.sender is self.desktop:
            if not event.type.startswith("object:children-changed:"):
                return
            if (
                    event.type == "object:children-changed:add"
                    and event.any_data.get_role() == Atspi.Role.APPLICATION
                    and event.any_data.get_name() == self.appname
            ):
                if self.app != None:
                    logger.warning("Ignoring extra '%s' app" % self.appname)
                    return
                logger.info("App '%s' intercepted" % self.appname)
                self.app = event.any_data

                tkname = self.app.get_toolkit_name()
                tkvers = self.app.get_toolkit_version()
                logger.info("toolkit: %s version %s" % (tkname, tkvers))
                if tkname == "GAIL": # Gtk v2
                    self.tk = GtkToolkit()
                elif tkname == "gtk": # v3 (or more?)
                    self.tk = GtkToolkit()
                elif tkname == "Qt":
                    #if tkvers.startswith("4."):
                    self.tk = QtToolkit()
                if self.tk == None:
                    raise RuntimeError("Unsupported toolkit")

                self.tk.tkversion = tkvers.split('.')

                # Qt5 won't notify us at all in Xvfb if window is already there
                self._registerAccessible(self.app)

            elif event.type == "object:children-changed:remove":
                self.log_event(event)
                logger.debug("del-window %x '%s'" % (id(event.source),
                                                     event.source.get_accessible_id()))
                # WTF cannot make a link with recorded app ?
                # To determine if it is the one that exited, check if our app is still there
                for i, child in [(i, self.desktop.get_child_at_index(i))
                                 for i in range(self.desktop.get_child_count())]:
                    if self.app is child:
                        break
                else:
                    logger.info("app terminated")
                    set_result(self.app_terminated, True)
            else:
                self.log_event(event)

        elif event.sender is self.app:
            self.log_event(event)

            # check if we already have an Asynccessible for objects
            # seen for the first time
            if event.source.path not in self._accessibles:
                self._registerAccessible(event.source)
            # FIXME don't if children-changed.remove ?
            obj = event.any_data
            if isinstance(obj, Atspi.Accessible) and obj.path not in self._accessibles:
                self._registerAccessible(obj)

            if event.type.startswith("object:children-changed:"):
                if event.type == "object:children-changed:remove":
                    self.unregister(event.any_data)

            elif (event.type == "object:state-changed:active" or
                  event.type == "object:state-changed:showing"):
                visible = {0: False, 1: True}[event.detail1]
                if event.source.path in self._accessibles:
                    ae = self._accessibles[event.source.path]
                    if ae.role == Atspi.Role.DIALOG: # FIXME why?
                        if visible:
                            # we may have noticed this at register time, don't notify twice
                            if not ae.visible:
                                assert False # FIXME broken branch - cannot happen ?
                                ae.publish_show(event.source)
                        else:
                            ae.publish_hide(event.source)

    def log_event(self, event):
        #logger.debug("event %s" % event)
        if event.sender is self.desktop:
            pfx = "dsk"
        elif event.sender is self.app:
            pfx = "app"
        else:
            logger.debug("evt sent by %s" % event.sender)
            pfx = "^^^"

        logger.debug(
            ("{0}: {1.type}({1.detail1}, {1.detail2}, {2}" +
             " src={3}").format(
                 pfx, event,
                 (self.printable_accessible(event.any_data)
                  if isinstance(event.any_data, Atspi.Accessible) else event.any_data),
                 self.printable_accessible(event.source)))
        if isinstance(event.any_data, Atspi.Accessible):
            a = event.any_data
            logger.debug(" data path={0.path}".format(a))

    def expected_node(self, pattern):
        pat = Pattern(self, pattern)
        assert pat not in self.__expected_patterns, f"{pattern} already expected"
        self.__expected_patterns.add(pat)
        for ae in self._accessibles.values():
            if pat.check_match(ae):
                logger.info("%s already appeared", pat)
                break
        else:
            logger.info("waiting for %s", pat)
        return pat

    def forget_pattern(self, pattern):
        assert pattern in self.__expected_patterns, f"{pattern} not expected"
        self.__expected_patterns.remove(pattern)
        logger.debug("forgetting %s", pattern)

    def unregister(self, accessible):
        "Record disparition of accessible"
        if accessible == None or accessible.path not in self._accessibles:
            return # we don't track everything
        ae = self._accessibles[accessible.path]
        logger.debug("unregister %s" % accessible.path)
        if ae.visible:
            ae.publish_hide(accessible)
        del self._accessibles[accessible.path]

    async def launch_gui(self, title, cmd, add_env=None):
        ae = self.expected_node([(Atspi.Role.FRAME, title)])

        if add_env == None:
            env = None
        else:
            env = os.environ
            env.update(add_env)

        proc = await asyncio.create_subprocess_shell(
            cmd, env=env,
            stdout=None,
            stderr=None)
        logger.info("process launched, expect window %r", title)
        return proc, ae

    @classmethod
    def _printable_nodepath_item(cls, item):
        if item is None:
            return None
        role = None if item[0] is None else item[0].value_nick
        name = item[1]
        if len(item) > 2:
            if isinstance(item[2], str):
                labels = item[2]
            else:
                labels = [str(label) for label in item[2]]
        else:
            labels = None

        return (role, name) + ((labels,) if labels else ())

    @classmethod
    def _printable_nodepath(cls, nodepath):
        return [cls._printable_nodepath_item(item) for item in nodepath]

    def printable_accessible(self, accessible):
        try:
            return str(self.asynccessible(accessible))
        except KeyError:
            states = accessible.get_state_set()
            if states.contains(Atspi.StateType.DEFUNCT):
                # with luck they got cached by Atspi
                role = accessible.role
                name = accessible.name
            else:
                role = accessible.get_role()
                name = accessible.get_name()
            return f"<{role.value_nick} {accessible.path} | {name}>"

class GtkToolkit(object):
    tkversion = None # filled at runtime
    def button_click(self, ae):
        ae.doAction("click")

class QtToolkit(object):
    tkversion = None # filled at runtime
    def button_click(self, ae):
        ae.doAction("Press")
