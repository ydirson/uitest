import sys

def test_qt(tk):
    app = tk.QtWidgets.QApplication([sys.argv[0]])

    window = tk.QtWidgets.QMainWindow()
    window.setWindowTitle("test app")

    vbox = tk.QtWidgets.QWidget(window)
    vboxlayout = tk.QtWidgets.QVBoxLayout(vbox)
    window.setCentralWidget(vbox)

    def open_action():
        dlg = tk.QtWidgets.QDialog(window)
        dlg.setWindowTitle("test dialog")
        layout = tk.QtWidgets.QVBoxLayout(dlg)

        button = tk.QtWidgets.QPushButton(dlg)
        button.setText("Close")
        layout.addWidget(button)
        button.clicked.connect(dlg.reject)

        dlg.show()

    button = tk.QtWidgets.QPushButton(vbox)
    button.setText("Open")
    button.clicked.connect(open_action)
    vboxlayout.addWidget(button)

    def add_action():
        button = tk.QtWidgets.QPushButton(vbox)
        button.setText("No-op")
        button.clicked.connect(open_action)
        vboxlayout.addWidget(button)

    button = tk.QtWidgets.QPushButton(vbox)
    button.setText("Add")
    button.clicked.connect(add_action)
    vboxlayout.addWidget(button)

    button = tk.QtWidgets.QPushButton(vbox)
    button.setText("Quit")
    button.clicked.connect(app.closeAllWindows)
    vboxlayout.addWidget(button)

    window.show()

    sys.exit(app.exec_())
