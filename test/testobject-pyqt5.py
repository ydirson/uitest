#!/usr/bin/env python3

import sip
sip.setapi('QString', 2)
sip.setapi('QVariant', 2)
import PyQt5 as PyQt
from PyQt5 import QtCore, QtGui, QtWidgets
QAction = QtWidgets.QAction

class namespace(object):
    pass

tk = namespace()
tk.PyQt = PyQt
tk.QtCore = QtCore
tk.QtGui = QtGui
tk.QtWidgets = QtWidgets
tk.QAction = QAction

from testlib_qt import test_qt

test_qt(tk)
