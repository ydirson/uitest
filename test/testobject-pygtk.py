#!/usr/bin/env python2

import gtk

window = gtk.Window()
window.connect("destroy", lambda w: gtk.main_quit())
window.set_title("test app")
window.set_default_size(200, 200)

vbox = gtk.VBox()
window.add(vbox)

def button_action(w):
    dlg = gtk.Dialog("test dialog",
                     buttons=(gtk.STOCK_OK, gtk.RESPONSE_ACCEPT))
    button = gtk.Button(label="Close")
    def close_dlg(w):
        dlg.hide()
        dlg.destroy()
    button.connect("clicked", close_dlg)
    dlg.vbox.pack_start(button, expand=False)
    dlg.show_all()

button = gtk.Button(label="Open")
button.connect("clicked", button_action)
vbox.add(button)

button = gtk.Button(label="Quit")
button.connect("clicked", lambda w: gtk.main_quit())
vbox.add(button)

window.show_all()
gtk.main()
