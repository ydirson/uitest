#!/usr/bin/env python3

# teach where to find a14y
import os, sys
testdir = os.path.dirname(sys.argv[0])
topdir = os.path.dirname(testdir)
sys.path[0:0] = [topdir]

import a14y2

import argparse
import asyncio
import logging

logger = logging.getLogger("main")

async def testoneprog(atspi, python, testprog):
    script = os.path.join(testdir, testprog)
    env = os.environ
    env.update(LC_ALL='C', LANGUAGE='',
               QT_LINUX_ACCESSIBILITY_ALWAYS_ON='1',
               GTK_MODULES='gail:atk-bridge')

    desktop = await atspi.get_desktop()
    await desktop.get_children() # request updates, FIXME

    logger.info(f"launching {python} {script!r}")
    proc = await asyncio.create_subprocess_exec(
        python, script, env=env, stdout=None, stderr=None)

    app = await desktop.notify_app_add(testprog)
    logger.info(f"got app {app}")

    proc.kill()

    await desktop.notify_app_remove(app)
    logger.info(f"app {app} was closed")

async def main():
    atspi = a14y2.Atspi()

    for python, testprog in (('/usr/bin/python2', 'testobject-pygtk.py'),
                             ('/usr/bin/python3', 'testobject-gtk3.py'),
                             ('/usr/bin/python3', 'testobject-pyqt5.py'),
                             ('/usr/bin/python3', 'testobject-pyside2.py'),
                             ('../env/qt6/bin/python3', 'testobject-pyside6.py'),
                             ):
        await testoneprog(atspi, python, testprog)

def cli_parser():
    cli_parser = argparse.ArgumentParser(
        description="A14y self-test")
    cli_parser.add_argument('-v', '--verbose', action="count", default=0,
                            help='increase verbosity level')
    return cli_parser

if __name__ == '__main__':
    args = cli_parser().parse_args()
    if args.verbose == 0:
        loglevel = logging.WARNING
    elif args.verbose == 1:
        loglevel = logging.INFO
    else:
        loglevel = logging.DEBUG

    logging.basicConfig(
        level=loglevel,
        format='{asctime}|{levelname}|{name}: {message}', style='{')
    logging.getLogger("asyncio").setLevel(loglevel)

    asyncio.get_event_loop().set_debug(True)
    asyncio.get_event_loop().run_until_complete(main())
