#!/usr/bin/env python3

import PySide6 as PyQt
from PySide6 import QtCore, QtGui, QtWidgets
QAction = QtGui.QAction

class namespace(object):
    pass

tk = namespace()
tk.PyQt = PyQt
tk.QtCore = QtCore
tk.QtGui = QtGui
tk.QtWidgets = QtWidgets
tk.QAction = QAction

from testlib_qt import test_qt

test_qt(tk)
