#!/usr/bin/env python3

import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk

def create_ui(app):
    window = Gtk.Window()
    app.add_window(window)
    window.connect("destroy", lambda w: app.quit())
    window.set_title("test app")
    window.set_default_size(200, 200)

    vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
    window.set_child(vbox)

    def open_action(w):
        dlg = Gtk.Dialog()
        dlg.set_title("test dialog")
        dlg.add_button("OK", Gtk.ResponseType.OK)
        button = Gtk.Button(label="Close")
        def close_dlg(w):
            dlg.hide()
            dlg.destroy()
        button.connect("clicked", close_dlg)
        dlg.get_content_area().append(button)
        dlg.set_transient_for(window)
        dlg.show()

    button = Gtk.Button(label="Open")
    button.connect("clicked", open_action)
    vbox.append(button)

    def add_action(w):
        button = Gtk.Button(label="No-op")
        vbox.append(button)

    button = Gtk.Button(label="Add")
    button.connect("clicked", add_action)
    vbox.append(button)

    button = Gtk.Button(label="Quit")
    button.connect("clicked", lambda w: app.quit())
    vbox.append(button)

    window.show()

app = Gtk.Application()
app.connect("activate", create_ui)
app.run()
